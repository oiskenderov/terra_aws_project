provider "aws" {
  region = "your-region" 
# change to our region
}

resource "aws_lambda_function" "mongo_lambda" {
  # Define your Lambda function configuration
  # ...

  environment {
    variables = {
      MONGO_URI = "your-mongo-uri" 
# change to our mongo URI
    }
  }
}

# Ensure Lambda has necessary permissions to access MongoDB
