provider "aws" {
  region = "your-region"
# change to our region
}

resource "aws_amplify_app" "my_amplify_app" {
  name     = "my-amplify-app"
  repository = "your-repository-url"
# change to our repo
}

resource "aws_amplify_domain" "my_amplify_domain" {
  domain_name = "my-amplify-domain"
  app_id      = aws_amplify_app.my_amplify_app.id
  sub_domain  = "www"
# change to our sub domain
}

resource "aws_amplify_branch" "main" {
  app_id = aws_amplify_app.my_amplify_app.id
  branch_name = "main"
}

# Build settings for Amplify
resource "aws_amplify_app_branch" "main_build" {
  app_id     = aws_amplify_app.my_amplify_app.id
  branch_name = aws_amplify_branch.main.name

  build_spec = <<EOF
version: 0.2
frontend:
  phases:
    build:
      commands:
        - npm ci
        - npm run build
  artifacts:
    baseDirectory: build
    files:
      - '**/*'
  cache:
    paths:
      - node_modules/**/*
EOF
}

# Environment variables for Amplify
resource "aws_amplify_app_environment_variable" "my_environment_variables" {
  app_id = aws_amplify_app.my_amplify_app.id
  env_vars = {
    REACT_APP_API_URL = "https://api.example.com",
    REACT_APP_ENV = "production",
    # Add other environment variables as needed
  }
}
