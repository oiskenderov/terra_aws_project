provider "aws" {
  region = "your-region" 
# change to our region
}

resource "aws_wafregional_web_acl" "web_acl" {
  # Define your WAF rules and conditions
  name        = "my-web-acl"
  metric_name = "my-web-acl-metric"

  # Define rules, conditions, etc.
  # ...

  # Attach ACL to resources (e.g., CloudFront distribution)
  depends_on = [aws_cloudfront_distribution.my_distribution]
}

resource "aws_cloudfront_distribution" "my_distribution" {
  # Define your CloudFront distribution configuration
  # ...

  # Attach WAF ACL to the distribution
  web_acl_id = aws_wafregional_web_acl.web_acl.id
}
