provider "aws" {
  region = "your-region" 
# change to our region
}

resource "aws_cognito_user_pool" "my_user_pool" {
  # Define your Cognito User Pool configuration
  # ...

  schema {
    # Define user pool schema
  }
}

resource "aws_cognito_identity_pool" "my_identity_pool" {
  # Define your Cognito Identity Pool configuration
  # ...

  cognito_identity_providers {
    # Define provider settings
  }
}
