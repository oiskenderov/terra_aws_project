provider "aws" {
  region = "your-region" 
# change to our region
}

resource "aws_lambda_function" "sqs_lambda" {
  # Define your Lambda function configuration
  # ...

  environment {
    variables = {
      SQS_QUEUE_URL = "your-sqs-queue-url" 
# change to our URL
    }
  }
}

# Ensure Lambda has necessary permissions to access SQS
