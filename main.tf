resource "aws_amplify_app" "amplify_app" {
  name       = var.app_name
  repository = var.repository
  oauth_token = var.token  
}
resource "aws_amplify_branch" "amplify_branch" {
  app_id      = aws_amplify_app.amplify_app.id
  branch_name = var.branch_name
}
resource "aws_amplify_domain_association" "domain_association" {
  app_id      = aws_amplify_app.amplify_app.id
  domain_name = var.domain_name
  wait_for_verification = false

  sub_domain {
    branch_name = aws_amplify_branch.amplify_branch.branch_name
    prefix      = var.branch_name
  }

}

module "waf" {
  source = "./modules/waf"
  # Add required variables
}

module "amplify" {
  source = "./modules/amplify"
  # Add required variables
}

module "cognito" {
  source = "./modules/cognito"
  # Add required variables
}

module "api_forwarder" {
  source = "./modules/api_forwarder"
  # Add required variables
}

module "lambda_sqs" {
  source = "./modules/lambda_sqs"
  # Add required variables
}

module "lambda_mongo" {
  source = "./modules/lambda_mongo"
  # Add required variables
}
